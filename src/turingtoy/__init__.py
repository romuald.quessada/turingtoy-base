from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


def run_turing_machine(
    machine: Dict,
    input_: str,
    num_steps: Optional[int] = None,
) -> Tuple[str, List[Dict], bool]:

    index = 0
    step_count = 0
    blank_symbol = machine["blank"]
    transition_table = machine["table"]
    current_state = machine["start state"]
    tape = list(input_)
    execution_history = []
    accepted = False

    while num_steps is None or step_count <= num_steps:
        instructions = transition_table[current_state][tape[index]]
        if isinstance(instructions, str):
            instructions = [instructions]

        execution_history.append({
            "state": current_state,
            "reading": tape[index],
            "position": index,
            "memory": "".join(tape),
            "transition": instructions
        })

        for instruction in instructions:
            if instruction == "write":
                tape[index] = instructions["write"]
            elif instruction == "R":
                index += 1
                if index == len(tape):
                    tape.append(blank_symbol)
                if isinstance(instructions, dict):
                    current_state = instructions["R"]
            elif instruction == "L":
                if index == 0:
                    tape.insert(0, blank_symbol)
                else:
                    index -= 1
                if isinstance(instructions, dict):
                    current_state = instructions["L"]

        step_count += 1

        if current_state in machine["final states"]:
            accepted = True
            break

    output = "".join(tape).strip(blank_symbol)
    return output, execution_history, accepted

